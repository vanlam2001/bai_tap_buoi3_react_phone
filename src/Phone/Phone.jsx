import React, { Component } from "react";
import { data_phone } from "./data_phone";
import DanhSach from "./DanhSach";
import ChiTietSP from "./ChiTietSP";

export default class Phone extends Component {
  state = {
    DanhSach: data_phone,
    ChiTietSP: data_phone,
  };

  XemSp = (SP) => {
    this.setState({ ChiTietSP: SP });
  };
  render() {
    return (
      <div>
        <DanhSach XemSp={this.XemSp} list={this.state.DanhSach}></DanhSach>
        <ChiTietSP ChiTietSP={this.state.ChiTietSP}></ChiTietSP>
      </div>
    );
  }
}
