import React, { Component } from "react";
import Item from "./Item";
export default class DanhSach extends Component {
  renderDanhSachDienThoai = () => {
    return this.props.list.map((item, index) => {
      return <Item XemSp={this.props.XemSp} data={item} key={index}></Item>;
    });
  };
  render() {
    return (
      <div className="container">
        <div className="row">{this.renderDanhSachDienThoai()}</div>;
      </div>
    );
  }
}
